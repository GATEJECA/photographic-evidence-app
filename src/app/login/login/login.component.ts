import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "@envs/environment";
import { ToastrService } from "ngx-toastr";
import { DataProvider } from "src/app/core/providers/data.provider";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent {
  public loading = false;
  constructor(
    private readonly router: Router,
    private readonly httpClient: HttpClient,
    private readonly toastr: ToastrService,
    private readonly dataProvider: DataProvider
  ) {}
  async onLogin({ user, password }) {
    try {
      this.loading = true;
      const response: any = await this.httpClient
        .post(`${environment.mainUrl}/api/v1/auth/token`, { user, password })
        .toPromise();
      const data = this.dataProvider.getData();
      data.token = response.data.token;
      await this.dataProvider.setData(data);
      const responseUser: any = await this.httpClient
        .get(`${environment.mainUrl}/api/v1/auth/me`)
        .toPromise();
      data.user = responseUser.data.user;
      await this.dataProvider.setData(data);
      this.router.navigate(["/admin"]);
    } catch (e) {
      let message = "Error";
      if (e.error?.data && Object.keys(e.error.data)) {
        message = e.error.data[Object.keys(e.error.data)[0]][0];
      } else if (e.error?.message) {
        message = e.error.message;
      } else {
        console.error(e);
      }
      this.toastr.error(message, "Error");
    } finally {
      this.loading = false;
    }
  }
}
