import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FlexLayoutModule } from "@angular/flex-layout";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { RouterTabs } from "./router-tab/router-tabs.directive";
import { RouterTab } from "./router-tab/router-tab.directive";
import { TranslateModule } from "@ngx-translate/core";
import { ModalLoadingComponent } from "./modal-loading/modal-loading.component";
import { NgxSmartModalModule } from "ngx-smart-modal";

@NgModule({
  imports: [CommonModule, RouterModule, FlexLayoutModule],
  declarations: [
    PageNotFoundComponent,
    RouterTabs,
    RouterTab,
    ModalLoadingComponent,
  ],
  exports: [
    CommonModule,
    FlexLayoutModule,
    NgxSmartModalModule,
    PageNotFoundComponent,
    RouterTabs,
    RouterTab,
    TranslateModule,
    ModalLoadingComponent,
  ],
})
export class SharedModule {}
