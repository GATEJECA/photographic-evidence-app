import { Component, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { DataProvider } from "src/app/core/providers/data.provider";

@Component({
  selector: "app-top-nav",
  templateUrl: "./top-nav.component.html",
  styleUrls: ["./top-nav.component.scss"],
})
export class TopNavComponent {
  @Output() sideNavToggled = new EventEmitter<void>();

  constructor(
    private readonly router: Router,
    private readonly dataProvider: DataProvider,
    private readonly translateService: TranslateService
  ) {}

  toggleSidebar() {
    this.sideNavToggled.emit();
  }

  async onLoggedout() {
    const data = this.dataProvider.getData();
    data.user = undefined;
    data.token = undefined;
    await this.dataProvider.setData(data);
    this.router.navigate(["/"]);
  }

  async changeLanguage(): Promise<void> {
    const lang = this.translateService.getDefaultLang() === "en" ? "es" : "en";
    this.translateService.setDefaultLang(lang);
    this.dataProvider.setLang(lang);
  }
}
