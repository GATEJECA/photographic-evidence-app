import { HttpClient } from "@angular/common/http";
import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { environment } from "@envs/environment";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-tables",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.scss"],
})
export class UsersComponent implements OnInit, AfterViewInit {
  displayedColumns = ["id", "name", "nickname", "type"];
  dataSource: MatTableDataSource<any>;
  loading = false;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private readonly httpClient: HttpClient,
    private readonly toastr: ToastrService,
    public ngxSmartModalService: NgxSmartModalService
  ) {}
  async ngOnInit() {
    let users = [];
    this.loading = true;
    try {
      const response: any = await this.httpClient
        .get(`${environment.mainUrl}/api/v1/users`)
        .toPromise();
      users = response.data.users;
    } catch (e) {
      let message = "Error";
      if (e.error?.data && Object.keys(e.error.data)) {
        message = e.error.data[Object.keys(e.error.data)[0]][0];
      } else if (e.error?.message) {
        message = e.error.message;
      } else {
        console.error(e);
      }
      this.toastr.error(message, "Error");
    } finally {
      this.loading = false;
    }
    this.dataSource = new MatTableDataSource(users);
  }

  async ngAfterViewInit() {
    if (this.dataSource) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  public async createUser(user, password, type, name) {
    try {
      this.loading = true;
      await this.httpClient
        .post(`${environment.mainUrl}/api/v1/users`, {
          name,
          nickname: user,
          type,
          password,
        })
        .toPromise();
      location.reload();
    } catch (e) {
      let message = "Error";
      if (e.error?.data && Object.keys(e.error.data)) {
        message = e.error.data[Object.keys(e.error.data)[0]][0];
      } else if (e.error?.message) {
        message = e.error.message;
      } else {
        console.error(e);
      }
      this.toastr.error(message, "Error");
    } finally {
      this.loading = false;
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
