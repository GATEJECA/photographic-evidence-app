import { HttpClient } from "@angular/common/http";
import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { environment } from "@envs/environment";
import { Lightbox } from "ngx-lightbox";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ToastrService } from "ngx-toastr";
import * as moment from "moment";

@Component({
  selector: "app-evidences",
  templateUrl: "./evidences.component.html",
  styleUrls: ["./evidences.component.scss"],
})
export class EvidencesComponent implements OnInit, AfterViewInit {
  displayedColumns = [
    "id",
    "evidence_date",
    "description",
    "category",
    "image",
  ];
  dataSource: MatTableDataSource<any>;
  loading = false;
  @ViewChild("fileInput") fileInput;
  file: File | null = null;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  evidences = [];
  constructor(
    private readonly httpClient: HttpClient,
    private readonly toastr: ToastrService,
    public ngxSmartModalService: NgxSmartModalService,
    private _lightbox: Lightbox
  ) {}
  async ngOnInit() {
    this.loading = true;
    try {
      const response: any = await this.httpClient
        .get(`${environment.mainUrl}/api/v1/evidences/my-evidences`)
        .toPromise();
      this.evidences = response.data.evidences.map((evidence) => ({
        id: evidence.id,
        evidence_date: evidence.evidence_date,
        description: evidence.description,
        category: evidence.category.category,
        src: evidence.fullUrl,
        caption: evidence.description,
      }));
    } catch (e) {
      let message = "Error";
      if (e.error?.data && Object.keys(e.error.data)) {
        message = e.error.data[Object.keys(e.error.data)[0]][0];
      } else if (e.error?.message) {
        message = e.error.message;
      } else {
        console.error(e);
      }
      this.toastr.error(message, "Error");
    } finally {
      this.loading = false;
    }
    this.dataSource = new MatTableDataSource(this.evidences);
  }

  async ngAfterViewInit() {
    if (this.dataSource) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  public async createEvidence(descripction, dateFinal, category, file) {
    const evidenceDate = moment(dateFinal, "MM/DD/YYYY").format("YYYY-MM-DD");
    const formData = new FormData();
    formData.append("image", file);
    formData.append("description", descripction);
    formData.append("category", category);
    formData.append("evidenced", evidenceDate);
    try {
      this.loading = true;
      await this.httpClient
        .post(`${environment.mainUrl}/api/v1/evidences`, formData)
        .toPromise();
      location.reload();
    } catch (e) {
      let message = "Error";
      if (e.error?.data && Object.keys(e.error.data)) {
        message = e.error.data[Object.keys(e.error.data)[0]][0];
      } else if (e.error?.message) {
        message = e.error.message;
      } else {
        console.error(e);
      }
      this.toastr.error(message, "Error");
    } finally {
      this.loading = false;
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  close(): void {
    this._lightbox.close();
  }

  showImage(id) {
    const index = this.evidences.findIndex((evidence) => evidence.id === id);
    if (index !== -1) {
      this._lightbox.open(this.evidences, 0);
    }
  }

  onClickFileInputButton(): void {
    this.fileInput.nativeElement.click();
  }

  onChangeFileInput(): void {
    const files: { [key: string]: File } = this.fileInput.nativeElement.files;
    this.file = files[0];
  }
}
