import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EvidencesComponent } from './evidences/evidences.component';

const routes: Routes = [
  {
    path: '',
    component: EvidencesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvidencesRoutingModule {}
