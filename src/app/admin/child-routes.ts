export const childRoutes = [
  {
    path: "",
    loadChildren: () =>
      import("./dashboard/dashboard.module").then((m) => m.DashboardModule),
    data: { icon: "dashboard", text: "dashboardSideMenu" },
  },
  {
    path: "evidences",
    loadChildren: () =>
      import("./evidences/evidences.module").then((m) => m.EvidencesModule),
    data: { icon: "perm_media", text: "evidencesSideMenu" },
  },
  {
    path: "users",
    loadChildren: () =>
      import("./users/users.module").then((m) => m.UsersModule),
    data: { icon: "people", text: "usersSideMenu" },
  },
];
