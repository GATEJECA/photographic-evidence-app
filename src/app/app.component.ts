import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { DataProvider } from "./core/providers/data.provider";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
})
export class AppComponent implements OnInit {
  constructor(
    private readonly translateService: TranslateService,
    private readonly dataProvider: DataProvider
  ) {}

  async ngOnInit(): Promise<void> {
    const { lang } = this.dataProvider.getData();
    this.translateService.setDefaultLang(lang);
  }
}
