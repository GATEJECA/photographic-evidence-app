import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { NgxGalleryImage, NgxGalleryOptions } from "@kolkov/ngx-gallery";
import { TranslateService } from "@ngx-translate/core";
import { ToastrService } from "ngx-toastr";
import { environment } from "@envs/environment";
import { DataProvider } from "src/app/core/providers/data.provider";

@Component({
  selector: "app-login",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.scss"],
})
export class MainComponent implements OnInit {
  constructor(
    private readonly translateService: TranslateService,
    private readonly httpClient: HttpClient,
    private readonly dataProvider: DataProvider,
    private readonly toastr: ToastrService
  ) {}
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  loading = false;
  async ngOnInit() {
    this.galleryOptions = [
      { imageSize: "contain" },
      {
        breakpoint: 500,
        width: "300px",
        height: "300px",
        thumbnailsColumns: 3,
      },
      {
        breakpoint: 300,
        width: "100%",
        height: "200px",
        thumbnailsColumns: 2,
      },
    ];
    try {
      this.loading = true;
      const response: any = await this.httpClient
        .get(`${environment.mainUrl}/api/v1/evidences`)
        .toPromise();
      this.galleryImages = response.data.evidences.map((evidence) => ({
        small: evidence.fullUrl,
        medium: evidence.fullUrl,
        big: evidence.fullUrl,
      }));
    } catch (e) {
      let message = "Error";
      if (e.error?.data && Object.keys(e.error.data)) {
        message = e.error.data[Object.keys(e.error.data)[0]][0];
      } else if (e.error?.message) {
        message = e.error.message;
      } else {
        console.error(e);
      }
      this.toastr.error(message, "Error");
    } finally {
      this.loading = false;
    }
  }
  async changeLanguage(): Promise<void> {
    const lang = this.translateService.getDefaultLang() === "en" ? "es" : "en";
    this.translateService.setDefaultLang(lang);
    this.dataProvider.setLang(lang);
  }
}
