import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MainRoutingModule } from "./main-routing.module";
import { MainComponent } from "./main/main.component";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatIconModule } from "@angular/material/icon";
import { SharedModule } from "../shared/shared.module";
import { NgxGalleryModule } from "@kolkov/ngx-gallery";

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MainRoutingModule,
    MatIconModule,
    SharedModule,
    NgxGalleryModule,
  ],
  declarations: [MainComponent],
})
export class MainModule {}
