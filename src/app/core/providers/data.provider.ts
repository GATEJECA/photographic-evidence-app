import { Injectable } from "@angular/core";
import asyncLocalStorage from "../../shared/util/async-local-storage";
import { SessionModel } from "../models/session.model";

@Injectable()
export class DataProvider {
  private readonly initialData: SessionModel = {
    token: undefined,
    user: undefined,
    lang: "en",
  };
  private data: SessionModel;
  constructor() {
    this.data = { ...this.initialData };
  }
  public getData(): SessionModel {
    return this.data;
  }
  public async clearData() {
    await asyncLocalStorage.clearItem("travel");
    this.data = { ...this.initialData };
  }
  public async setData(data: SessionModel) {
    await asyncLocalStorage.setItem("gendra", JSON.stringify(data));
    this.data = data;
  }
  public async load() {
    const data = JSON.parse(await asyncLocalStorage.getItem("gendra"));
    if (data) {
      this.data = data;
    }
  }
  public async setLang(lang: "es" | "en") {
    this.data.lang = lang === "es" ? "es" : "en";
    return this.setData(this.data);
  }
}
