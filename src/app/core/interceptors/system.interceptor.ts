import { Injectable } from "@angular/core";
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
} from "@angular/common/http";
import { throwError, Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { DataProvider } from "../providers/data.provider";
import { Router } from "@angular/router";
import { SessionModel } from "../models/session.model";

@Injectable()
export class SystemInterceptor implements HttpInterceptor {
  private AUTH_HEADER = "Authorization";
  constructor(
    private readonly dataProvider: DataProvider,
    private readonly router: Router
  ) {}
  public intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const data = this.dataProvider.getData();
    req = req.clone({
      headers: req.headers.set("lang", data.lang),
    });
    if (!req.headers.has("Accept")) {
      req = req.clone({
        headers: req.headers.set("Accept", "application/json"),
      });
    }
    req = this.addAuthenticationToken(req, data);
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error && error.status === 401) {
          this.router.navigate(["/login"]);
        }
        return throwError(error);
      })
    );
  }
  private addAuthenticationToken(
    request: HttpRequest<any>,
    data: SessionModel
  ): HttpRequest<any> {
    const token = data.token;
    if (!token) {
      return request;
    }
    return request.clone({
      headers: request.headers.set(this.AUTH_HEADER, `Bearer ${token}`),
    });
  }
}
