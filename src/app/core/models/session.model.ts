export class SessionModel {
  token: string;
  user: any;
  lang: "es" | "en";
}
